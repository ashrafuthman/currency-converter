Hey Johannes,

I have started with:
1. Writing down how I can complete the logic of the converting using the API
2. I have decided to write down which components from Material UI I want to work with it and O managed to take the
- Text fields
- Grids
- Icons
- Modal
- AppBar
3. Let us get our hands dirty! The logic of the functionality where I managed to deal with the actual converting took me 30 -35 mins
4. After knowing what I wanted to use from Material UI it was like a puzzle so it was easy because I also managed on a paper which design I want to reach.

** I did not manage to add all the currencies because it will be a lot of time for me to add their currencies icons or as text.
** I would also like to find another api when it changes every second to make the value changes live to the user (It is really easy thing to do)
** I would also add CSS files to each component if I had more time

I really would like you to give me feedback as soon as possible and I really want to have the next session if I will pass before the end of this week because I would 
like to start soon with you(Beginning of August).

Best regards,

Ashraf Uthman