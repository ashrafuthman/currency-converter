import React from 'react';
import CurrencyConverter from './Containers/CurrencyConverter';
import Navbar from './Components/Navbar';
import Modal from './Components/Modal';

function App() {
  return (
    <React.Fragment>
      <Navbar />
      <div className="App">

        <CurrencyConverter />
      </div>
      <Modal />
    </React.Fragment>
  );
}

export default App;
