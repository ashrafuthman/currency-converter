import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MedikuraLogo from '../Assets/MedikuraLogo.png';
import Dollar from '@material-ui/icons/AttachMoney'

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

export default function Navbar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{backgroundColor: '#35818f', position: 'relative'}}>
        <Toolbar>
          <Typography variant="h4" color="inherit">
            <img src={MedikuraLogo}/>
          </Typography>
        </Toolbar>
        <Dollar style={{ position: 'absolute', right: '1%', bottom: '30%', fontSize: '30px'}}/>
      </AppBar>

    </div>
  );
}