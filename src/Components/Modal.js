import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import Smile from '@material-ui/icons/SentimentVerySatisfied';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    left: '52% !important',
    top: '50%',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    outline: 'none',
  },
}));

function SimpleModal() {
  const [open, setOpen] = React.useState(false);
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const classes = useStyles();

  return (
    <div>
      <Button onClick={handleOpen}
        style={{
        border: '1px solid #35818f',
        color: '#35818f',
        position: 'absolute',
        bottom: 10,
        right: 10
        }}
      >       
        Click Me!
      </Button>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <div style={modalStyle} className={classes.paper}>
          It was really fun working with the test you 
          <br/>
          gave me and I really hope that I will pass it
          <br/> 
          and get the opportunity to prove in person what
          <br/>
          what I am capable of. 
          <br/>
          Have a great day and keep on smiling and staying
          <br/>
          positive
          <br/>
          <Smile style={{fontSize: 25}}/>
        </div>
      </Modal>
    </div>
  );
}

export default SimpleModal;