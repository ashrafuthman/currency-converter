import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Loop from '@material-ui/icons/Loop';


import '../App.css';


class CurrencyConverter extends Component {
    
    // State of the values we will use for our Webapp and we fill them wether with
    // empty or default values

    state = {
        base: 'EUR',
        currencies: [
            {
              value: 'USD',
              label: '$ Dollar',
            },
            {
              value: 'EUR',
              label: '€ Euro',
            },

            {
              value: 'JPY',
              label: '¥ Yen',
            }
        ],
        convertTo: 'USD',
        amount: 0,
        result: '',
        date: ''


    }

    componentDidMount = () => {
        // It is the safest place in the React Cycle to call an API to get a Data when the component mounts
        axios.get('https://api.exchangeratesapi.io/latest')
                .then(response => {
                    console.log(response.data.date  );
                    let finalDate = response.data.date
                    this.setState({
                        date: finalDate
                    })
                })
                .catch(error => {
                    console.log(error);
                });
    }

    ChangeCurrencyHandler = async (event, text) => {
        // I merged 3 functions in one sending texts with each function and with that text we decide which case the fuction should execute
        if (text == 'base') {     
            let newBase = await event.target.value
            this.setState({ 
                base: newBase
            })
        console.log(this.state.base)
        } else if (text == 'convertTo') {
            let newConvertTo = await event.target.value
        
            this.setState({ 
                convertTo: newConvertTo
            })
            console.log(this.state.convertTo)
        } else {
            
            this.setState({
                amount: event.target.value
            })
        }
        
    }


    ConvertIt = () => {
        // Avoid error case else GET the api
        if (this.state.base == this.state.convertTo) {
            alert('Please make sure you have two different values')
        } else {
            // Save the result in the state
            axios.get('https://api.exchangeratesapi.io/latest?base=' + this.state.base)
                .then(response => {
                    console.log(response.data.rates[this.state.convertTo]  );
                    let finalResult = response.data.rates[this.state.convertTo] * this.state.amount
                    this.setState({
                        result: finalResult
                    })
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }

    switchCurrencies = async() => {
        // We make sure to save the correct variables and then simply switch them
        let newBase = await this.state.convertTo
        let newConvertTo = await this.state.base
        this.setState({
            base: newBase,
            convertTo: newConvertTo,
    
        })
    }

    render() {
        
        return (
           
            <Paper
                style={{
                    position: 'relative',
                    width: 650
                }}
            >
                
                <form>
                    <Grid container spacing={3}>
                        
                        <Grid item xs={12}>
                                <h2
                                    style={{
                                        textAlign: 'center'
                                    }}
                                >
                                    Currency Converter
                                </h2>
                        </Grid>

                    </Grid>

                    <Grid container spacing={3}
                        style={{
                            display: 'flex'
                        }}
                    >
                        
                        <Grid item xs={12}
                            style={{
                                display: 'flex'
                            }}
                            justify="center"
                        >
                            <TextField
                                id="standard-number"
                                label="Amount"
                                pattern="[0-9]*"
                                value={this.state.amount}
                                onChange={(event) => this.ChangeCurrencyHandler(event, 'amount')}
                                type="number"
                                onKeyDown={e => /[\+\-\.\,]$/.test(e.key) && e.preventDefault()}
                                margin="normal"
                            />
                        </Grid>

                    </Grid>

                    <Grid container spacing={3}>
                        
                        <Grid spacing={10} item xs={4}
                            style={{
                                display: 'flex'
                            }}
        
                            justify="center"
                        >
                            <TextField
                                id="standard-select-currency-native"
                                select
                                style={{
                                    width: 90,                          
                                }}  
                                onChange = {(event) => this.ChangeCurrencyHandler(event, 'base')}
                                value={this.state.base}
                                SelectProps={{
                                native: true,                  
                                }}
                                helperText="Please select your currency"
                                margin="normal"
                            >
                                {this.state.currencies.map(option => (
                                    <option key={option.value} value={option.value}  >
                                        {option.label}
                                    </option>
                                ))}
                            </TextField>
                        </Grid>

                        <Grid spacing={10} item xs={4}>
                            <div
                                style={{
                                    position: 'relative',
                                    top: '25%'
                                }}
                            >
                                <Button
                                    onClick={() => this.switchCurrencies()}
                                >
                                    <Loop  style={{fontSize: 50}} />
                                </Button>
                                
                            </div>
                            
                        </Grid>

                        <Grid spacing={10} item xs={4}
                            style={{
                                display: 'flex'
                            }}
        
                            justify="center"
                        >     
                            <TextField
                                id="standard-select-currency-native"
                                select
                                style={{
                                    width: 90
                                }}
                                onChange = {(event) => this.ChangeCurrencyHandler(event, 'convertTo')}
                                value={this.state.convertTo}
                                SelectProps={{
                                native: true,
                                    
                                }}
                                helperText="The currency you want to convert to"
                                margin="normal"
                            >
                                {this.state.currencies.map(option => (
                                    <option key={option.value} value={option.value}  >
                                        {option.label}
                                    </option>
                                ))}
                            </TextField>
                        </Grid>

                    </Grid>

                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <h2
                                style={{
                                    textAlign: 'center'
                                }}
                        >
                                Result: {this.state.result}
                            </h2>
                        </Grid>
                    </Grid>

                </form>

                <Button 
                        variant="contained"
                        color="primary"
                        style={{
                            position: 'absolute',
                            right: '10px',
                            bottom: '10px'
                        }}
                        onClick={() => this.ConvertIt()}
                    >
                        Convert
                </Button>

                <p 
                        variant="contained"
                        color="primary"
                        style={{
                            position: 'absolute',
                            left: '10px',
                            bottom: '0px',

                        }}
                        onClick={() => this.ConvertIt()}
                    >
                        Last time update: {this.state.date}
                </p>
                
            </Paper>
        );
    }
}

export default CurrencyConverter;
